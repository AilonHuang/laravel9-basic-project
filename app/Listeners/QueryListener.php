<?php

namespace App\Listeners;

use Illuminate\Database\Events\QueryExecuted;
use Illuminate\Support\Facades\Log;

class QueryListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * 监听sql语句
     * Handle the event
     * @param QueryExecuted $event
     * @return void
     */
    public function handle(QueryExecuted $event)
    {
        if (config('app.debug') === false) {
            return;
        }
        // 如果是测试环境或者本地环境则打印sql、参数、执行时间等信息到log日志
        $sql = $event->sql;
        $bindings = $event->bindings;
        $time = $event->time; // 毫秒
        $bindings = array_map(function ($binding) {
            if (is_string($binding)) {
                return (string)$binding;
            }
            if ($binding instanceof \DateTime) {
                return $binding->format("'Y-m-d H:i:s'");
            }
            return $binding;
        }, $bindings);
        $sql = str_replace('?', '%s', $sql);
        $sql = sprintf($sql, ...$bindings);
        Log::info('sql_log', ['sql' => $sql, 'time' => $time . 'ms']);
    }
}
